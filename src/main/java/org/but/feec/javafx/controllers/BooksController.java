package org.but.feec.javafx.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import org.but.feec.javafx.api.BookBasicView;
import org.but.feec.javafx.api.ReaderBasicView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class BooksController {
    private static final Logger logger = LoggerFactory.getLogger(BooksController.class);

    @FXML
    private TableColumn<BookBasicView, Integer> booksId;
    @FXML
    private TableColumn<BookBasicView, String> booksName;
    @FXML
    private TableColumn<BookBasicView, String> booksAuthorName;
    @FXML
    private TableColumn<BookBasicView, String> booksAuthorSurname;
    @FXML
    private TableColumn<BookBasicView, String> booksReleaseDate;
    @FXML
    private TableColumn<BookBasicView, Integer> booksPages;

    @FXML
    private TableView<BookBasicView> systemBooksTableView;

    private ObservableList<BookBasicView> observableList;

    public ObservableList<BookBasicView> getObservableList() {return observableList;}
    public void setObservableList(ObservableList<BookBasicView> observableList) {this.observableList = observableList;}

    public BooksController() {
    }

    @FXML
    private void initialize() {
        booksId.setCellValueFactory(new PropertyValueFactory<BookBasicView, Integer>("id"));
        booksName.setCellValueFactory(new PropertyValueFactory<BookBasicView, String>("bookName"));
        booksAuthorName.setCellValueFactory(new PropertyValueFactory<BookBasicView, String>("authorName"));
        booksAuthorSurname.setCellValueFactory(new PropertyValueFactory<BookBasicView, String>("authorSurname"));
        booksReleaseDate.setCellValueFactory(new PropertyValueFactory<BookBasicView, String>("releaseDate"));
        booksPages.setCellValueFactory(new PropertyValueFactory<BookBasicView, Integer>("pages"));

        systemBooksTableView.setItems(getObservableList());

        logger.info("ReadersController initialized");
    }

    public void booksList (List<BookBasicView> listOfBooks) {
        ObservableList<BookBasicView> books = FXCollections.observableArrayList();
        books.addAll(listOfBooks);
        setObservableList(books);
    }


}
