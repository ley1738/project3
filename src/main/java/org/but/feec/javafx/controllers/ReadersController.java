package org.but.feec.javafx.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import org.but.feec.javafx.api.ReaderBasicView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.*;

public class ReadersController {
    private static final Logger logger = LoggerFactory.getLogger(ReadersController.class);

    @FXML
    private TableColumn<ReaderBasicView, Integer> readersId;
    @FXML
    private TableColumn<ReaderBasicView, String> readersName;
    @FXML
    private TableColumn<ReaderBasicView, String> readersSurname;
    @FXML
    private TableColumn<ReaderBasicView, String> readersEmail;
    @FXML
    private TableView<ReaderBasicView> systemReadersTableView;


    public Stage stage;
    private ObservableList<ReaderBasicView> observableList;

    public ObservableList<ReaderBasicView> getObservableList() {return observableList;}
    public void setObservableList(ObservableList<ReaderBasicView> observableList) {this.observableList = observableList;}

    public void setStage(Stage stage) {this.stage = stage;}

    public ReadersController() {
    }

    @FXML
    private void initialize() {
        readersId.setCellValueFactory(new PropertyValueFactory<ReaderBasicView, Integer>("id"));
        readersName.setCellValueFactory(new PropertyValueFactory<ReaderBasicView, String>("givenName"));
        readersSurname.setCellValueFactory(new PropertyValueFactory<ReaderBasicView, String>("familyName"));
        readersEmail.setCellValueFactory(new PropertyValueFactory<ReaderBasicView, String>("email"));

        systemReadersTableView.setItems(getObservableList());

        logger.info("ReadersController initialized");
    }


    public void readersList (List<ReaderBasicView> listOfReaders) {
        ObservableList<ReaderBasicView> readers = FXCollections.observableArrayList();
        readers.addAll(listOfReaders);
        setObservableList(readers);
    }
}
