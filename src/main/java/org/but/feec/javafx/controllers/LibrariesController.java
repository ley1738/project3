package org.but.feec.javafx.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import org.but.feec.javafx.App;
import org.but.feec.javafx.api.BookBasicView;
import org.but.feec.javafx.api.LibraryView;
import org.but.feec.javafx.api.ReaderBasicView;
import org.but.feec.javafx.data.LibraryRepository;
import org.but.feec.javafx.exceptions.ExceptionHandler;
import org.but.feec.javafx.services.LibraryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;
import java.util.List;

public class LibrariesController {

    private static final Logger logger = LoggerFactory.getLogger(ReadersController.class);

    @FXML
    private TextField searchTextField;
    @FXML
    public Button refreshBtn;
    @FXML
    private TableColumn<LibraryView, Long> libraryIdColumn;
    @FXML
    private TableColumn<LibraryView, String> libraryNameColumn;
    @FXML
    private TableColumn<LibraryView, String> libraryCityColumn;
    @FXML
    private TableView<LibraryView> libraryTableView;

    private LibraryService libraryService;
    private LibraryRepository libraryRepository;

    public LibrariesController() {
    }

    @FXML
    private void initialize() {
        libraryRepository = new LibraryRepository();
        libraryService = new LibraryService(libraryRepository);

        libraryIdColumn.setCellValueFactory(new PropertyValueFactory<LibraryView, Long>("id"));
        libraryNameColumn.setCellValueFactory(new PropertyValueFactory<LibraryView, String>("name"));
        libraryCityColumn.setCellValueFactory(new PropertyValueFactory<LibraryView, String>("city"));

        ObservableList<LibraryView> observableLibrariesList = initializeLibrariesData();
        libraryTableView.setItems(observableLibrariesList);

        initializeFilteredLibrariesData(observableLibrariesList);

        libraryTableView.getSortOrder().add(libraryIdColumn);
        initializeTableViewSelection();

        logger.info("LibrariesController initialized");
    }

    private void initializeTableViewSelection() {
        MenuItem books = new MenuItem("View books in library");
        MenuItem readers = new MenuItem("View readers in library");

        books.setOnAction((ActionEvent event) -> {
             LibraryView libraryView = libraryTableView.getSelectionModel().getSelectedItem();
             try {
                 FXMLLoader fxmlLoader = new FXMLLoader();
                 fxmlLoader.setLocation(App.class.getResource("fxml/book-view.fxml"));
                 Stage stage = new Stage();

                 Integer libraryId = libraryView.getId().intValue();

                 //tk
                 System.out.println("library id: " + libraryId);
                 List<BookBasicView> booksView = libraryService.getBooksBasicView(libraryId);
                 booksView.forEach(System.out::println);

                 stage.setUserData(booksView);
                 stage.setTitle("Books View");

                 BooksController controller = new BooksController();
                 controller.booksList(booksView);
                 fxmlLoader.setController(controller);

                 Scene scene = new Scene(fxmlLoader.load(), 755, 420);
                 stage.setScene(scene);
                 stage.show();

             }   catch (IOException ex) {
                 ExceptionHandler.handleException(ex);
             }
        });

        readers.setOnAction((ActionEvent event) -> {
            LibraryView libraryView = libraryTableView.getSelectionModel().getSelectedItem();
            try {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(App.class.getResource("fxml/reader-view.fxml"));
                Stage stage = new Stage();

                Integer libraryId = libraryView.getId().intValue();
                //tk
                System.out.println("library id: " + libraryId);
                List<ReaderBasicView> readersView = libraryService.getReadersBasicView(libraryId);
                readersView.forEach(System.out::println);

                stage.setUserData(readersView);
                stage.setTitle("Readers View");

                ReadersController controller = new ReadersController();
                controller.readersList(readersView);
                fxmlLoader.setController(controller);

                Scene scene = new Scene(fxmlLoader.load(), 600, 420);
                stage.setScene(scene);
                stage.show();

            } catch (IOException ex) {
                ExceptionHandler.handleException(ex);
            }
        });
        ContextMenu menu = new ContextMenu();
        menu.getItems().add(books);
        menu.getItems().add(readers);
        libraryTableView.setContextMenu(menu);
    }

    private ObservableList<LibraryView> initializeLibrariesData() {
        List<LibraryView> libraries = libraryService.getLibrariesView();
        return FXCollections.observableArrayList(libraries);
    }

    private void initializeFilteredLibrariesData(ObservableList<LibraryView> librariesData){
        FilteredList<LibraryView> filteredLibraries = new FilteredList<>(librariesData, b ->true);
        System.out.println(filteredLibraries);
        searchTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredLibraries.setPredicate(librarySearch -> {
                if (newValue.isEmpty() || newValue.isBlank() || newValue == null ){
                    return true;
                }
                String searchedWord = newValue.toLowerCase();
                if (librarySearch.getCity().toLowerCase().indexOf(searchedWord) > -1) {
                    return true;
                }else {
                    return false;
                }
            });
        });
        SortedList<LibraryView> sortedLibraries = new SortedList<>(filteredLibraries);
        sortedLibraries.comparatorProperty().bind(libraryTableView.comparatorProperty());
        libraryTableView.setItems(sortedLibraries);
    }

    public void handleRefreshBtn(ActionEvent actionEvent) {
        ObservableList<LibraryView> observableLibrariesList = initializeLibrariesData();
        libraryTableView.setItems(observableLibrariesList);

        initializeFilteredLibrariesData(observableLibrariesList);

        libraryTableView.refresh();
        libraryTableView.sort();
    }
}
