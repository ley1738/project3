package org.but.feec.javafx.controllers;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.but.feec.javafx.api.BookDeleteView;
import org.but.feec.javafx.api.SBookView;
import org.but.feec.javafx.data.SBookRepository;
import org.but.feec.javafx.services.SBookService;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

public class BookDeleteController {
    private static final Logger logger = LoggerFactory.getLogger(BookDeleteController.class);

    @FXML
    public Button deleteBookButton;
    @FXML
    public TextField idTextField;
    @FXML
    private TextField nameTextField;
    @FXML
    private TextField pagesTextField;
    @FXML
    private TextField statusTextField;

    private SBookService bookService;
    private SBookRepository bookRepository;
    private ValidationSupport validation;

    public Stage stage;
    public void setStage(Stage stage){this.stage = stage;}

    @FXML
    public void initialize() {
        bookRepository = new SBookRepository();
        bookService = new SBookService(bookRepository);

        validation = new ValidationSupport();
        validation.registerValidator(idTextField, Validator.createEmptyValidator("The id must not be empty."));
        idTextField.setEditable(false);
        validation.registerValidator(nameTextField, Validator.createEmptyValidator("The name must not be empty."));
        nameTextField.setEditable(false);
        validation.registerValidator(pagesTextField, Validator.createEmptyValidator("The pages must not be empty."));
        pagesTextField.setEditable(false);
        validation.registerValidator(statusTextField, Validator.createEmptyValidator("The status must not be empty."));
        statusTextField.setEditable(false);

        deleteBookButton.disableProperty().bind(validation.invalidProperty());

        loadBooksData();

        logger.info("BookDeleteController initialized");
    }

    private void loadBooksData() {
        Stage stage = this.stage;
        if (stage.getUserData() instanceof SBookView) {
            SBookView bookView = (SBookView) stage.getUserData();
            idTextField.setText(String.valueOf(bookView.getId()));
            nameTextField.setText(bookView.getBookName());
            pagesTextField.setText(String.valueOf(bookView.getPages()));
            statusTextField.setText(bookView.getStatus());
        }
    }

    @FXML
    public void handleBookDeleteButton (ActionEvent event) {
        Integer id = Integer.valueOf(idTextField.getText());

        BookDeleteView bookDeleteView = new BookDeleteView();
        bookDeleteView.setId(id);

        bookService.deleteBook(bookDeleteView);

        bookDeletedConfirmationDialog();
    }

    private void bookDeletedConfirmationDialog() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Book Deleted Confirmation");
        alert.setHeaderText("Your book was successfully deleted.");

        Timeline idlestage = new Timeline(new KeyFrame(Duration.seconds(1), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                alert.setResult(ButtonType.CANCEL);
                alert.hide();
            }
        }));
        idlestage.setCycleCount(1);
        idlestage.play();
        Optional<ButtonType> result = alert.showAndWait();
    }


}
