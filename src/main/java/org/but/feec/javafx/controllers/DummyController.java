package org.but.feec.javafx.controllers;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.util.Duration;
import org.but.feec.javafx.api.DummyView;
import org.but.feec.javafx.data.DummyRepository;
import org.but.feec.javafx.services.DummyService;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

public class DummyController {
    private static final Logger logger = LoggerFactory.getLogger(DummyController.class);

    @FXML
    private Button addDataButton;
    @FXML
    private TextField dataTextField;

    private DummyService dummyService;
    private DummyRepository dummyRepository;
    private ValidationSupport validation;

    @FXML
    public void initialize() {
        dummyRepository = new DummyRepository();
        dummyService = new DummyService(dummyRepository);

        validation = new ValidationSupport();
        validation.registerValidator(dataTextField, Validator.createEmptyValidator("The id must not be empty."));

        addDataButton.disableProperty().bind(validation.invalidProperty());

        logger.info("BookEditController initialized");
    }

    @FXML
    public void handleAddDataButton (ActionEvent event) {
        String id = dataTextField.getText();

        DummyView dummyView = new DummyView();
        dummyView.setId(id);

        dummyService.dummyInjection(dummyView);

        dummyInjectionConfirmationDialog();
    }

    private void dummyInjectionConfirmationDialog() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Dummy Injection Confirmation");
        alert.setHeaderText("It was selected from dummy table.");

        Timeline idlestage = new Timeline(new KeyFrame(Duration.seconds(1), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                alert.setResult(ButtonType.CANCEL);
                alert.hide();
            }
        }));
        idlestage.setCycleCount(1);
        idlestage.play();
        Optional<ButtonType> result = alert.showAndWait();
    }


}
