package org.but.feec.javafx.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import org.but.feec.javafx.App;
import org.but.feec.javafx.api.SBookView;
import org.but.feec.javafx.data.SBookRepository;
import org.but.feec.javafx.exceptions.ExceptionHandler;
import org.but.feec.javafx.services.SBookService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

public class SBooksController {
    private static final Logger logger = LoggerFactory.getLogger(SBooksController.class);

    @FXML
    public Button addBookButton;
    @FXML
    public Button refreshBtn;
    @FXML
    private TableColumn<SBookView, Integer> bookIdColumn;
    @FXML
    private TableColumn<SBookView, String> bookNameColumn;
    @FXML
    private TableColumn<SBookView, Integer> bookPagesColumn;
    @FXML
    private TableColumn<SBookView, String> bookStatusColumn;

    @FXML
    private TableView<SBookView> booksTableView;


    private SBookService bookService;
    private SBookRepository bookRepository;

    public SBooksController() {
    }

    @FXML
    private void initialize() {
        bookRepository = new SBookRepository();
        bookService = new SBookService(bookRepository);

        bookIdColumn.setCellValueFactory(new PropertyValueFactory<SBookView, Integer>("id"));
        bookNameColumn.setCellValueFactory(new PropertyValueFactory<SBookView, String>("bookName"));
        bookPagesColumn.setCellValueFactory(new PropertyValueFactory<SBookView, Integer>("pages"));
        bookStatusColumn.setCellValueFactory(new PropertyValueFactory<SBookView, String>("status"));

        ObservableList<SBookView> observableBooksList = initializeBooksData();
        booksTableView.setItems(observableBooksList);

        booksTableView.getSortOrder().add(bookIdColumn);

        initializeTableViewSelection();

        logger.info("BooksController initialized");
    }

    private void initializeTableViewSelection() {
        MenuItem edit = new MenuItem("Edit book");
        MenuItem delete = new MenuItem("Delete book");
        edit.setOnAction((ActionEvent event) -> {
            SBookView bookView = booksTableView.getSelectionModel().getSelectedItem();
            try {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(App.class.getResource("fxml/book-edit.fxml"));
                Stage stage = new Stage();
                stage.setUserData(bookView);
                stage.setTitle("Book Edit");

                BookEditController controller = new BookEditController();
                controller.setStage(stage);
                fxmlLoader.setController(controller);

                Scene scene = new Scene(fxmlLoader.load(), 650, 400);
                stage.setScene(scene);

                stage.show();
            } catch (IOException ex) {
                ExceptionHandler.handleException(ex);
            }
        });

        delete.setOnAction((ActionEvent event) -> {
            SBookView bookView = booksTableView.getSelectionModel().getSelectedItem();
            try {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(App.class.getResource("fxml/book-delete.fxml"));
                Stage stage = new Stage();
                stage.setUserData(bookView);
                stage.setTitle("Book Delete");

                BookDeleteController controller = new BookDeleteController();
                controller.setStage(stage);
                fxmlLoader.setController(controller);

                Scene scene = new Scene(fxmlLoader.load(), 600, 400);
                stage.setScene(scene);

                stage.show();
            } catch (IOException ex) {
                ExceptionHandler.handleException(ex);
            }
        });


        ContextMenu menu = new ContextMenu();
        menu.getItems().add(edit);
        menu.getItems().addAll(delete);//addAll?
        booksTableView.setContextMenu(menu);
    }

    private ObservableList<SBookView> initializeBooksData() {
        List<SBookView> books = bookService.getBookView();
        return FXCollections.observableArrayList(books);
    }

    public void handleAddBookButton(ActionEvent actionEvent) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(App.class.getResource("fxml/book-create.fxml"));
            Scene scene = new Scene(fxmlLoader.load(), 600, 300);
            Stage stage = new Stage();
            stage.setTitle("Create Book");
            stage.setScene(scene);
            stage.show();
        } catch (IOException ex) {
            ExceptionHandler.handleException(ex);
        }
    }

    public void handleRefreshButton(ActionEvent actionEvent) {
        ObservableList<SBookView> observableBooksList = initializeBooksData();
        booksTableView.setItems(observableBooksList);
        booksTableView.refresh();
        booksTableView.sort();
    }

}
