package org.but.feec.javafx.controllers;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.but.feec.javafx.api.BookEditView;
import org.but.feec.javafx.api.SBookView;
import org.but.feec.javafx.data.SBookRepository;
import org.but.feec.javafx.services.SBookService;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

public class BookEditController {
    private static final Logger logger = LoggerFactory.getLogger(BookEditController.class);

    @FXML
    public Button editBookButton;
    @FXML
    public TextField idTextField;
    @FXML
    private TextField nameTextField;
    @FXML
    private TextField pagesTextField;
    @FXML
    private ChoiceBox statusChoiceBox;

    private SBookService bookService;
    private SBookRepository bookRepository;
    private ValidationSupport validation;

    public Stage stage;
    public void setStage(Stage stage){this.stage = stage;}

    @FXML
    public void initialize() {
        bookRepository = new SBookRepository();
        bookService = new SBookService(bookRepository);

        validation = new ValidationSupport();
        validation.registerValidator(idTextField, Validator.createEmptyValidator("The id must not be empty."));
        idTextField.setEditable(false);
        validation.registerValidator(nameTextField, Validator.createEmptyValidator("The name must not be empty."));
        validation.registerValidator(pagesTextField, Validator.createEmptyValidator("The pages must not be empty."));

        editBookButton.disableProperty().bind(validation.invalidProperty());

        loadBooksData();

        logger.info("BookEditController initialized");
    }

    private void loadBooksData() {
        Stage stage = this.stage;
        if (stage.getUserData() instanceof SBookView) {
            SBookView bookView = (SBookView) stage.getUserData();
            idTextField.setText(String.valueOf(bookView.getId()));
            nameTextField.setText(bookView.getBookName());
            pagesTextField.setText(String.valueOf(bookView.getPages()));
            statusChoiceBox.getItems().add("1");
            statusChoiceBox.getItems().add("2");
            statusChoiceBox.getItems().add("3");
            statusChoiceBox.getItems().add("4");
        }
    }

    @FXML
    public void handleBookEditButton (ActionEvent event) {
        Integer id = Integer.valueOf(idTextField.getText());
        String name = nameTextField.getText();
        Integer pages = Integer.valueOf(pagesTextField.getText());
        Integer status = Integer.valueOf(statusChoiceBox.getValue().toString());
        System.out.println("volba:     "+statusChoiceBox.getValue().toString());
        System.out.println("jmeno:     "+name);
        System.out.println("pages:     "+pagesTextField.getText());
        System.out.println("id:     "+idTextField.getText());

        BookEditView bookEditView = new BookEditView();
        bookEditView.setId(id);
        bookEditView.setName(name);
        bookEditView.setPages(pages);
        bookEditView.setStatus(status);

        bookService.editBook(bookEditView);
        System.out.println("editview:     "+bookEditView.toString());

        bookEditedConfirmationDialog();
    }

    private void bookEditedConfirmationDialog() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Book Edited Confirmation");
        alert.setHeaderText("Your book was successfully edited.");

        Timeline idlestage = new Timeline(new KeyFrame(Duration.seconds(1), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                alert.setResult(ButtonType.CANCEL);
                alert.hide();
            }
        }));
        idlestage.setCycleCount(1);
        idlestage.play();
        Optional<ButtonType> result = alert.showAndWait();
    }



}
