package org.but.feec.javafx.controllers;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.util.Duration;
import org.but.feec.javafx.api.BookCreateView;
import org.but.feec.javafx.data.SBookRepository;
import org.but.feec.javafx.services.SBookService;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

public class BookCreateController {
    private static final Logger logger = LoggerFactory.getLogger(BookCreateController.class);

    @FXML
    public Button newBookCreateButton;
    @FXML
    private TextField newBookName;
    @FXML
    private TextField newBookPages;

    private SBookService bookService;
    private SBookRepository bookRepository;
    private ValidationSupport validation;

    @FXML
    public void initialize() {
        bookRepository = new SBookRepository();
        bookService = new SBookService(bookRepository);

        validation = new ValidationSupport();
        validation.registerValidator(newBookName, Validator.createEmptyValidator("The book name must not be empty."));
        validation.registerValidator(newBookPages, Validator.createEmptyValidator("The book pages must not be empty."));

        newBookCreateButton.disableProperty().bind(validation.invalidProperty());

        logger.info("BookCreateController initialized");
    }

    @FXML
    void handleCreateNewBook(ActionEvent event) {
        String bookName = newBookName.getText();
        String book_pages = newBookPages.getText();
        Integer pages = Integer.valueOf(book_pages);

        BookCreateView bookCreateView = new BookCreateView();
        bookCreateView.setName(bookName);
        bookCreateView.setPages(pages);

        bookService.createBook(bookCreateView);

        bookCreatedConfirmationDialog();
    }

    private void bookCreatedConfirmationDialog() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Book Created Confirmation");
        alert.setHeaderText("Your book was successfully created.");

        Timeline idlestage = new Timeline(new KeyFrame(Duration.seconds(1), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                alert.setResult(ButtonType.CANCEL);
                alert.hide();
            }
        }));
        idlestage.setCycleCount(1);
        idlestage.play();
        Optional<ButtonType> result = alert.showAndWait();
    }

}
