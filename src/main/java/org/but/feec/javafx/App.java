package org.but.feec.javafx;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.but.feec.javafx.exceptions.ExceptionHandler;




//App.java
public class App extends Application {
    private FXMLLoader loader;
    private AnchorPane mainStage;

    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage primaryStage)  {
        try {
            loader = new FXMLLoader(getClass().getResource("login-view.fxml"));
            mainStage = loader.load();

            primaryStage.setTitle("LIBRARIES");
            Scene scene = new Scene(mainStage);

            primaryStage.setScene(scene);
            primaryStage.getIcons().add(new Image(App.class.getResourceAsStream("logos/library.jpg")));
            primaryStage.show();
        } catch (Exception ex) {
            ExceptionHandler.handleException(ex);
        }
    }
}

