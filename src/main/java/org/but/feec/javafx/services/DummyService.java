package org.but.feec.javafx.services;

import org.but.feec.javafx.api.DummyView;
import org.but.feec.javafx.data.DummyRepository;

public class DummyService {
    private DummyRepository dummyRepository;
    public DummyService(DummyRepository dummyRepository) {
        this.dummyRepository = dummyRepository;
    }

    public void dummyInjection(DummyView dummyView){
        dummyRepository.dummyInjection(dummyView);
    }
}
