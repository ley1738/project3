package org.but.feec.javafx.services;

import at.favre.lib.crypto.bcrypt.BCrypt;
import org.but.feec.javafx.api.ReaderAuthView;
import org.but.feec.javafx.data.ReaderRepository;
import org.but.feec.javafx.exceptions.ResourceNotFoundException;

public class AuthService {
    private ReaderRepository readerRepository;

    public AuthService(ReaderRepository readerRepository) {
        this.readerRepository = readerRepository;
    }

    private ReaderAuthView findReaderByEmail(String email) { return readerRepository.findReaderByEmail(email); }

    public boolean authenticate(String username, String password) {
        if (username == null || username.isEmpty() || password == null || password.isEmpty()) {
            return false;
        }
        ReaderAuthView readerAuthView = findReaderByEmail(username);
        if (readerAuthView == null) {
            throw new ResourceNotFoundException("Provided username is not found.");
        }
        BCrypt.Result result = BCrypt.verifyer().verify(password.toCharArray(), readerAuthView.getPassword());
        return result.verified;
    }

}
