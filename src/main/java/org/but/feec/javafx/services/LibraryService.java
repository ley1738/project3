package org.but.feec.javafx.services;

import org.but.feec.javafx.api.BookBasicView;
import org.but.feec.javafx.api.LibraryView;
import org.but.feec.javafx.api.ReaderBasicView;
import org.but.feec.javafx.data.LibraryRepository;


import java.util.List;

public class LibraryService {
    private LibraryRepository libraryRepository;

    public LibraryService(LibraryRepository libraryRepository) {
        this.libraryRepository = libraryRepository;
    }

    public List<LibraryView> getLibrariesView() {
        return libraryRepository.getLibrariesView();
    }

    //readers menu
    public List<ReaderBasicView> getReadersBasicView(Integer libraryId) {
        return libraryRepository.getReadersBasicView(libraryId);
    }
    //books menu
    public List<BookBasicView> getBooksBasicView(Integer libraryId) {
        return libraryRepository.getBooksBasicView(libraryId);
    }

}
