package org.but.feec.javafx.services;

import org.but.feec.javafx.api.BookCreateView;
import org.but.feec.javafx.api.BookDeleteView;
import org.but.feec.javafx.api.BookEditView;
import org.but.feec.javafx.api.SBookView;
import org.but.feec.javafx.data.SBookRepository;

import java.util.List;

public class SBookService {
    private SBookRepository bookRepository;

    public SBookService(SBookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    public List<SBookView> getBookView() {
        return bookRepository.getBookView();
    }

    public void createBook(BookCreateView bookCreateView){
        bookRepository.createBook(bookCreateView);
    }

    public void editBook(BookEditView bookEditView) {
        bookRepository.editBook(bookEditView);
    }

    public void deleteBook(BookDeleteView bookDeleteView) {
        bookRepository.deleteBook(bookDeleteView);
    }
}
