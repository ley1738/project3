package org.but.feec.javafx.data;

import org.but.feec.javafx.api.ReaderAuthView;
import org.but.feec.javafx.config.DataSourceConfig;
import org.but.feec.javafx.exceptions.DataAccessException;

import java.sql.*;

public class ReaderRepository {

    public ReaderAuthView findReaderByEmail(String email) {
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "SELECT email, password" +
                             " FROM sch.READER r" + //sch.READER
                             " WHERE r.email = ?")
        ) {
            preparedStatement.setString(1, email);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return mapToReaderAuth(resultSet);
                }
            }
        } catch (SQLException e) {
            throw new DataAccessException("Find reader by ID with addresses failed.", e);
        }
        return null;
    }

    private ReaderAuthView mapToReaderAuth(ResultSet rs) throws SQLException {
        ReaderAuthView reader = new ReaderAuthView();
        reader.setEmail(rs.getString("email"));
        reader.setPassword(rs.getString("password"));
        return reader;
    }

}
