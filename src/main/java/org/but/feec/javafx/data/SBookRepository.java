package org.but.feec.javafx.data;

import org.but.feec.javafx.api.BookCreateView;
import org.but.feec.javafx.api.BookDeleteView;
import org.but.feec.javafx.api.BookEditView;
import org.but.feec.javafx.api.SBookView;
import org.but.feec.javafx.config.DataSourceConfig;
import org.but.feec.javafx.exceptions.DataAccessException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


//book solo
public class SBookRepository {
    public List<SBookView> getBookView() {
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "SELECT book_id, book_name, pages, status_name" +
                             " FROM sch.book b" +
                             " LEFT JOIN sch.status s ON b.status_id = s.status_id");
             ResultSet resultSet = preparedStatement.executeQuery();) {
                List<SBookView> bookViews = new ArrayList<>();
                while (resultSet.next()) {
                    bookViews.add(mapToBookView(resultSet));
                }
                return bookViews;
        } catch (SQLException e) {
            throw new DataAccessException("Book view could not be loaded.", e);
        }
    }

    public void createBook(BookCreateView bookCreateView) {
        String insertBookSQL = "INSERT INTO sch.book (book_name, pages, status_id) VALUES (?,?,?)";
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(insertBookSQL, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, bookCreateView.getName());
            preparedStatement.setInt(2, bookCreateView.getPages());
            preparedStatement.setInt(3, 4);

            int affectedRows = preparedStatement.executeUpdate();

            if (affectedRows == 0) {
                throw new DataAccessException("Creating book failed, no rows affected.");
            }
        } catch (SQLException e) {
            throw new DataAccessException("Creating book failed operation on the database failed.");
        }
    }

    public void editBook(BookEditView bookEditView) {
        String insertPersonSQL = "UPDATE sch.book b SET book_name = ?, pages = ?, status_id = ? WHERE b.book_id = ?";
        String checkIfExists = "SELECT book_name FROM sch.book b WHERE b.book_id = ?";
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(insertPersonSQL, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, bookEditView.getName());
            preparedStatement.setInt(2, bookEditView.getPages());
            preparedStatement.setInt(3, bookEditView.getStatus());
            preparedStatement.setInt(4, bookEditView.getId());

            try {
                connection.setAutoCommit(false);
                try (PreparedStatement ps = connection.prepareStatement(checkIfExists, Statement.RETURN_GENERATED_KEYS)) {
                    ps.setInt(1, bookEditView.getId());
                    ps.execute();
                } catch (SQLException e) {
                    throw new DataAccessException("This book for edit do not exists.");
                }

                int affectedRows = preparedStatement.executeUpdate();
                if (affectedRows == 0) {
                    throw new DataAccessException("Editing book failed, no rows affected.");
                }
                connection.commit();
            } catch (SQLException e) {
                connection.rollback();
            } finally {
                connection.setAutoCommit(true);
            }
        } catch (SQLException e) {
            throw new DataAccessException("Editing book failed operation on the database failed.");
        }
    }

    public void deleteBook(BookDeleteView bookDeleteView) {
        String insertPersonSQL = "DELETE FROM sch.book b WHERE b.book_id = ?";
        String checkIfExists = "SELECT book_name FROM sch.book b WHERE b.book_id = ?";
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(insertPersonSQL, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setInt(1, bookDeleteView.getId());

            try {
                connection.setAutoCommit(false);
                try (PreparedStatement ps = connection.prepareStatement(checkIfExists, Statement.RETURN_GENERATED_KEYS)) {
                    ps.setInt(1, bookDeleteView.getId());
                    ps.execute();
                } catch (SQLException e) {
                    throw new DataAccessException("This book cannot be deleted.");
                }

                int affectedRows = preparedStatement.executeUpdate();
                if (affectedRows == 0) {
                    throw new DataAccessException("Deleting book failed, no rows affected.");
                }
                connection.commit();
            } catch (SQLException e) {
                connection.rollback();
            } finally {
                connection.setAutoCommit(true);
            }
        } catch (SQLException e) {
            throw new DataAccessException("Deleting book failed operation on the database failed.");
        }
    }


    private SBookView mapToBookView(ResultSet rs) throws SQLException {
        SBookView bookView = new SBookView();
        bookView.setId(rs.getInt("book_id"));
        bookView.setBookName(rs.getString("book_name"));
        bookView.setPages(rs.getInt("pages"));
        bookView.setStatus(rs.getString("status_name"));
        return bookView;
    }


}
