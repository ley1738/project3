package org.but.feec.javafx.data;

import org.but.feec.javafx.api.DummyView;
import org.but.feec.javafx.config.DataSourceConfig;
import org.but.feec.javafx.exceptions.DataAccessException;
import java.sql.*;

public class DummyRepository {
    public void dummyInjection(DummyView dummyView) {
        String sql = "SELECT * FROM sch.dummy d WHERE d.dummy_id = '" + dummyView.getId() + "';";
        try {
            Connection connection = DataSourceConfig.getConnection();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                System.out.println(resultSet.getInt("dummy_id") + "\t"+
                        resultSet.getString("data1") + "\t"+
                        resultSet.getString("data2") + "\t"+
                        resultSet.getString("data3"));
            }
            System.out.println("\n");

        } catch (SQLException e) {
            throw new DataAccessException("Dummy operation on the database failed.");
        }
    }
}
