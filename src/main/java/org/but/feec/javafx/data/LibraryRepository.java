package org.but.feec.javafx.data;

import org.but.feec.javafx.api.BookBasicView;
import org.but.feec.javafx.api.LibraryView;
import org.but.feec.javafx.api.ReaderBasicView;
import org.but.feec.javafx.config.DataSourceConfig;
import org.but.feec.javafx.exceptions.DataAccessException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class LibraryRepository {
    public List<LibraryView> getLibrariesView() {
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "SELECT library_id, library_name, city" +
                             " FROM sch.library l");
             ResultSet resultSet = preparedStatement.executeQuery();) {
            List<LibraryView> libraryViews = new ArrayList<>();
            while (resultSet.next()) {
                libraryViews.add(mapToLibraryView(resultSet));
            }
            return libraryViews;
        } catch (SQLException e) {
            throw new DataAccessException("Libraries view could not be loaded.", e);
        }
    }

    private LibraryView mapToLibraryView(ResultSet rs) throws SQLException {
        LibraryView libraryView = new LibraryView();
        libraryView.setId(rs.getLong("library_id"));
        libraryView.setName(rs.getString("library_name"));
        libraryView.setCity(rs.getString("city"));

        return libraryView;
    }

    //reader repository
    public List<ReaderBasicView> getReadersBasicView(Integer libraryId) {//Long libraryId
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "SELECT r.reader_id, given_name, family_name, email" +
                             " FROM sch.reader r" +
                             " LEFT JOIN sch.reader_library rl ON r.reader_id = rl.reader_id" +
                             " LEFT JOIN sch.library l ON rl.library_id = l.library_id" +
                             " WHERE l.library_id = ?");
             ){
            preparedStatement.setInt(1, libraryId);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<ReaderBasicView> readerBasicViews = new ArrayList<>();
            while (resultSet.next()) {
                readerBasicViews.add(mapToReaderBasicView(resultSet));
            }
            return readerBasicViews;

        } catch (SQLException e) {
            throw new DataAccessException("Readers basic view could not be loaded.", e);
        }
    }
    private ReaderBasicView mapToReaderBasicView(ResultSet rs) throws SQLException {
        ReaderBasicView readerBasicView = new ReaderBasicView();
        readerBasicView.setId(rs.getInt("reader_id"));
        readerBasicView.setGivenName(rs.getString("given_name"));
        readerBasicView.setFamilyName(rs.getString("family_name"));
        readerBasicView.setEmail(rs.getString("email"));
        return readerBasicView;
    }

    //book repository
    public List<BookBasicView> getBooksBasicView(Integer libraryId) {//Long libraryId
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "SELECT b.book_id, book_name, given_name, family_name, release_date, pages" +
                             " FROM sch.book b" +
                             " LEFT JOIN sch.author_book ab ON b.book_id = ab.book_id" +
                             " LEFT JOIN sch.author a ON ab.author_id = a.author_id" +
                             " LEFT JOIN sch.library_book lb ON b.book_id = lb.book_id" +
                             " LEFT JOIN sch.library l ON lb.library_id = l.library_id" +
                             " WHERE l.library_id = ?");
        ){
            preparedStatement.setInt(1, libraryId);
            ResultSet resultSet = preparedStatement.executeQuery();
            List<BookBasicView> bookBasicViews = new ArrayList<>();
            while (resultSet.next()) {
                bookBasicViews.add(mapToBookBasicView(resultSet));
            }
            return bookBasicViews;

        } catch (SQLException e) {
            throw new DataAccessException("Books basic view could not be loaded.", e);
        }
    }
    private BookBasicView mapToBookBasicView(ResultSet rs) throws SQLException {
        BookBasicView bookBasicView = new BookBasicView();
        bookBasicView.setId(rs.getInt("book_id"));
        bookBasicView.setBookName(rs.getString("book_name"));
        bookBasicView.setAuthorName(rs.getString("given_name"));
        bookBasicView.setAuthorSurname(rs.getString("family_name"));
        bookBasicView.setReleaseDate(rs.getString("release_date"));
        bookBasicView.setPages(rs.getInt("pages"));
        return bookBasicView;
    }


}
