package org.but.feec.javafx.api;

public class BookEditView {
    private Integer id;
    private String name;
    private Integer pages;
    private Integer status;

    public Integer getId() {return id;}
    public void setId(Integer id) {this.id = id;}

    public String getName() {return name;}
    public void setName(String name) {this.name = name;}

    public Integer getPages() {return pages;}
    public void setPages(Integer pages) {this.pages = pages;}

    public Integer getStatus() {return status;}
    public void setStatus(Integer status) {this.status = status;}

    @Override
    public String toString() {
        return "BookEditView{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", pages=" + pages +
                ", status=" + status +
                '}';
    }
}
