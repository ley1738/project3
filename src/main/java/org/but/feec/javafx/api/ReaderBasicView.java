package org.but.feec.javafx.api;

import javafx.beans.property.*;

import java.util.List;

public class ReaderBasicView {
    private IntegerProperty id = new SimpleIntegerProperty();
    private StringProperty givenName = new SimpleStringProperty();
    private StringProperty familyName = new SimpleStringProperty();
    private StringProperty email = new SimpleStringProperty();

    public Integer getId() { return idProperty().get();}
    public void setId(Integer id) {
        this.idProperty().setValue(id);
    }

    public String getGivenName() {return givenNameProperty().get();}
    public void setGivenName(String givenName) {
        this.givenNameProperty().setValue(givenName);
    }

    public String getFamilyName() {
        return familyNameProperty().get();
    }
    public void setFamilyName(String familyName) {
        this.familyNameProperty().setValue(familyName);
    }

    public String getEmail() {return emailProperty().get();}
    public void setEmail(String email) {
        this.emailProperty().setValue(email);
    }

    public IntegerProperty idProperty() {
        return id;
    }
    public StringProperty givenNameProperty() {
        return givenName;
    }
    public StringProperty familyNameProperty() {
        return familyName;
    }
    public StringProperty emailProperty() {
        return email;
    }

    @Override
    public String toString() {
        return "ReaderBasicView{" +
                "id=" + id +
                ", givenName=" + givenName +
                ", familyName=" + familyName +
                ", email=" + email +
                '}';
    }
}
