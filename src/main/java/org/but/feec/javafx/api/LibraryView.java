package org.but.feec.javafx.api;

import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class LibraryView {
    private LongProperty id = new SimpleLongProperty();
    private StringProperty name = new SimpleStringProperty();
    private StringProperty city = new SimpleStringProperty();

    public Long getId() {
        return idProperty().get();
    }
    public void setId(Long id) {
        this.idProperty().setValue(id);
    }

    public String getName() {
        return nameProperty().get();
    }
    public void setName(String name) {
        this.nameProperty().setValue(name);
    }

    public String getCity() { return cityProperty().get(); }
    public void setCity(String city) {
        this.cityProperty().setValue(city);
    }

    public LongProperty idProperty() { return id; }
    public StringProperty nameProperty() { return name; }
    public StringProperty cityProperty() { return city; }
}
