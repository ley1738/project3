package org.but.feec.javafx.api;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class SBookView {

    private IntegerProperty id = new SimpleIntegerProperty();
    private StringProperty bookName = new SimpleStringProperty();
    private IntegerProperty pages = new SimpleIntegerProperty();
    private StringProperty status = new SimpleStringProperty();

    public Integer getId() { return idProperty().get();}
    public void setId(Integer id) {
        this.idProperty().setValue(id);
    }

    public String getBookName() {return bookNameProperty().get();}
    public void setBookName(String bookName) {
        this.bookNameProperty().setValue(bookName);
    }

    public Integer getPages() { return pagesProperty().get();}
    public void setPages(Integer pages) {
        this.pagesProperty().setValue(pages);
    }

    public String getStatus() {
        return statusProperty().get();
    }
    public void setStatus(String status) {
        this.statusProperty().setValue(status);
    }

    public IntegerProperty idProperty() {
        return id;
    }
    public StringProperty bookNameProperty() {
        return bookName;
    }
    public IntegerProperty pagesProperty() {
        return pages;
    }
    public StringProperty statusProperty() { return status;}

}
