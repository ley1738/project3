package org.but.feec.javafx.api;

public class BookCreateView {

    private String name;
    private Integer pages;

    public String getName() {return name;}
    public void setName(String name) {this.name = name;}

    public Integer getPages() {return pages;}
    public void setPages(Integer pages) {this.pages = pages;}

    @Override
    public String toString() {
        return "BookCreateView{" +
                "name='" + name + '\'' +
                ", pages=" + pages +
                '}';
    }
}
