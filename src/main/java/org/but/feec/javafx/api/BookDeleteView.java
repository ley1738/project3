package org.but.feec.javafx.api;

public class BookDeleteView {
    private Integer id;


    public Integer getId() {return id;}
    public void setId(Integer id) {this.id = id;}


    @Override
    public String toString() {
        return "BookDeleteView{" +
                "id=" + id +
                '}';
    }
}
