package org.but.feec.javafx.api;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.util.List;

public class BookBasicView {
    private IntegerProperty id = new SimpleIntegerProperty();
    private StringProperty bookName = new SimpleStringProperty();
    private StringProperty authorName = new SimpleStringProperty();
    private StringProperty authorSurname = new SimpleStringProperty();
    private StringProperty releaseDate = new SimpleStringProperty();
    private IntegerProperty pages = new SimpleIntegerProperty();

    public Integer getId() { return idProperty().get();}
    public void setId(Integer id) {
        this.idProperty().setValue(id);
    }

    public String getBookName() {return bookNameProperty().get();}
    public void setBookName(String bookName) {
        this.bookNameProperty().setValue(bookName);
    }

    public String getAuthorName() {
        return authorNameProperty().get();
    }
    public void setAuthorName(String authorName) {
        this.authorNameProperty().setValue(authorName);
    }

    public String getAuthorSurname() {return authorSurnameProperty().get();}
    public void setAuthorSurname(String authorSurname) {
        this.authorSurnameProperty().setValue(authorSurname);
    }

    public String getReleaseDate() {return releaseDateProperty().get();}
    public void setReleaseDate(String releaseDate) {
        this.releaseDateProperty().setValue(releaseDate);
    }

    public Integer getPages() { return pagesProperty().get();}
    public void setPages(Integer pages) {
        this.pagesProperty().setValue(pages);
    }

    public IntegerProperty idProperty() {
        return id;
    }
    public StringProperty bookNameProperty() {
        return bookName;
    }
    public StringProperty authorNameProperty() {
        return authorName;
    }
    public StringProperty authorSurnameProperty() {
        return authorSurname;
    }
    public StringProperty releaseDateProperty() {
        return releaseDate;
    }
    public IntegerProperty pagesProperty() {
        return pages;
    }

    //todo
    private List<BookBasicView> listOfBooksBasicView;
    public List<BookBasicView> getListOfBooksBasicView() {
        return listOfBooksBasicView;
    }
    public void setListOfBooksBasicView(List<BookBasicView> listOfBooks) {
        this.listOfBooksBasicView = listOfBooksBasicView;
    }

    @Override
    public String toString() {
        return "BookBasicView{" +
                "id=" + id +
                ", bookName=" + bookName +
                ", authorName=" + authorName +
                ", authorSurname=" + authorSurname +
                ", releaseDate=" + releaseDate +
                ", pages=" + pages +
                ", listOfBooksBasicView=" + listOfBooksBasicView +
                '}';
    }
}
