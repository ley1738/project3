CREATE TABLE if not exists sch.LIBRARY (
  library_id serial not null,
  library_name varchar(45) not null,
  city varchar(40) not null,
  PRIMARY KEY (library_id)
);

CREATE TABLE if not exists sch.STATUS (
  status_id serial not null,
  status_name varchar(20) ,
  PRIMARY KEY (status_id)
);

CREATE TABLE if not exists sch.BOOK (
  book_id serial not null,
  book_name varchar(40) not null,
  release_date date not null,
  pages integer not null,
  status_id integer not null,
  PRIMARY KEY (book_id),
  CONSTRAINT FK_BOOK_status_id
    FOREIGN KEY (status_id)
      REFERENCES sch.STATUS(status_id)
      ON DELETE CASCADE
);

CREATE TABLE if not exists sch.ROLE (
  role_id serial not null,
  role_name varchar(20) ,
  PRIMARY KEY (role_id)
);


CREATE TABLE if not exists sch.TYPE (
  type_id serial not null,
  type_name varchar(20) not null,
  PRIMARY KEY (type_id)
);

CREATE TABLE if not exists sch.BOOK_TYPE (
  book_id integer not null,
  type_id integer not null,
  CONSTRAINT FK_BOOK_TYPE_book_id
    FOREIGN KEY (book_id)
      REFERENCES sch.BOOK(book_id)
	  ON DELETE CASCADE,
  CONSTRAINT FK_BOOK_TYPE_type_id
    FOREIGN KEY (type_id)
      REFERENCES sch.TYPE(type_id)
	  ON DELETE CASCADE
);

CREATE TABLE if not exists sch.MAILING_ADDRESS (
  address_id serial not null,
  city varchar(40) not null,
  street varchar(40) not null,
  house_number varchar(10),
  zip_code varchar(10),
  PRIMARY KEY (address_id)
);

CREATE TABLE if not exists sch.READER (
  reader_id serial not null,
  given_name varchar(20) not null,
  family_name varchar(20) not null,
  birth_date date,
  email varchar(45) not null,
  address_id integer,
  phone_number varchar(20),
  start_reg date not null,
  end_reg date not null,
  password varchar(300) not null,
  PRIMARY KEY (reader_id),
  CONSTRAINT FK_READER_address_id
    FOREIGN KEY (address_id)
      REFERENCES sch.MAILING_ADDRESS(address_id)
	  ON DELETE CASCADE
);

CREATE TABLE if not exists sch.READER_ROLE (
  reader_id integer not null,
  role_id integer not null,
  CONSTRAINT FK_READER_ROLE_reader_id
    FOREIGN KEY (reader_id)
      REFERENCES sch.READER(reader_id)
	  ON DELETE CASCADE,
  CONSTRAINT FK_READER_ROLE_role_id
    FOREIGN KEY (role_id)
      REFERENCES sch.ROLE(role_id)
	  ON DELETE CASCADE
);

CREATE TABLE if not exists sch.READER_LIBRARY (
  reader_id integer not null,
  library_id integer not null,
  CONSTRAINT FK_READER_LIBRARY_reader_id
    FOREIGN KEY (reader_id)
      REFERENCES sch.READER(reader_id)
	  ON DELETE CASCADE,
  CONSTRAINT FK_READER_LIBRARY_library_id
    FOREIGN KEY (library_id)
      REFERENCES sch.LIBRARY(library_id)
	  ON DELETE CASCADE
);


CREATE TABLE if not exists sch.LIBRARY_BOOK (
  library_id integer not null,
  book_id integer not null,
  CONSTRAINT FK_LIBRARY_BOOK_library_id
    FOREIGN KEY (library_id)
      REFERENCES sch.LIBRARY(library_id)
      ON DELETE CASCADE,
  CONSTRAINT FK_LIBRARY_BOOK_book_id
    FOREIGN KEY (book_id)
      REFERENCES sch.BOOK(book_id)
      ON DELETE CASCADE
);

CREATE TABLE if not exists sch.AUTHOR (
  author_id serial not null,
  given_name varchar(20) not null,
  family_name varchar(20) not null,
  birth_date date,
  description varchar(1000),
  PRIMARY KEY (author_id)
);


CREATE TABLE if not exists sch.READER_BOOK (
  reader_id integer not null,
  book_id integer not null,
  CONSTRAINT FK_READER_BOOK_reader_id
    FOREIGN KEY (reader_id)
      REFERENCES sch.READER(reader_id)
      ON DELETE CASCADE,
  CONSTRAINT FK_READER_BOOK_book_id
    FOREIGN KEY (book_id)
      REFERENCES sch.BOOK(book_id)
      ON DELETE CASCADE
);

CREATE TABLE if not exists sch.AUTHOR_BOOK (
  author_id integer not null,
  book_id integer not null,
  CONSTRAINT FK_AUTHOR_BOOK_author_id
    FOREIGN KEY (author_id)
      REFERENCES sch.AUTHOR(author_id)
	  ON DELETE CASCADE,
  CONSTRAINT FK_AUTHOR_BOOK_book_id
    FOREIGN KEY (book_id)
      REFERENCES sch.BOOK(book_id)
	  ON DELETE CASCADE
);

--injection
CREATE TABLE if not exists sch.DUMMY_TABLE1 (
  dummy_id serial not null,
  data1 varchar(40) not null,
  data2 varchar(40) not null,
  data3 varchar(40) not null,
  PRIMARY KEY (dummy_id)
);

CREATE TABLE if not exists sch.DUMMY_TABLE2 (
  dummy_id serial not null,
  data1 varchar(40) not null,
  data2 varchar(40) not null,
  data3 varchar(40) not null,
  PRIMARY KEY (dummy_id)
);

CREATE TABLE if not exists sch.DUMMY (
  dummy_id serial not null,
  data1 varchar(40) not null,
  data2 varchar(40) not null,
  data3 varchar(40) not null,
  PRIMARY KEY (dummy_id)
);